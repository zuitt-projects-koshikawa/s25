// Aggregation in MonggoDB

db.fruits.insertMany([
	{
	"name" : "Apple",
	"supplier" : "Red Farms Inc.",
	"stocks" : 20,
	"price" : 40,
	"onSale" : true,
	},
	{
	"name" : "Banana",
	"supplier" : "Yellow Farms",
	"stocks" : 15,
	"price" : 20,
	"onSale" : true,
	},
	{
	"name" : "Kiwi",
	"supplier" : "Green Farming and Canning",
	"stocks" : 25,
	"price" : 50,
	"onSale" : true,
	},
	{
	"name" : "Mango",
	"supplier" : "Yellow Farms",
	"stocks" : 10,
	"price" : 60,
	"onSale" : true,
	},
	{
	"name" : "Dragon Fruit",
	"supplier" : "Red Farms Inc.",
	"stocks" : 10,
	"price" : 60,
	"onSale" : true,
	},
])
// Aggregation in MongoDb
	// This is the act or process of generating manipulated data and perform operations to create filtered results that helps in data analysis.
	// This helps in creating reports from analyzing the data provided in our documents
	// This helps in arriving at summarized information NOT previously show in our documents.

	// Aggregation Pipelines
		// Aggretation is done in 2 to 3 steps. Each stage is called a pipeline.
		// The first pipeline in our example is with the use of the $match
		// $match is used to pass the document or get the documents that will pass our condition
			// syntax: {$match: {field: value}}

		// The group pipeline in our example is with the use of $group
		// $group grouped elements/documents together and create an analysis of these grouped documents
			// {$group: {_id:<id>, fieldResult: "valueResult"}}
			// _id: this is us associating an id for aggregated result
				// When id is provided with supplier, we created aggregated results per supplier.
			// $sum will get the total of all the values of the given field. This will allow us to get the total of the values of the given field per group. It will non-numerical values.


db.fruits.aggregate([
		{$match:{"onSale" : true}},
		{$group: {_id: "$supplier", totalStocks: {$sum:"$stocks"}}}
])

db.fruits.aggregate([
	{$match: {"onSale" : true}},
	{$group: {_id: "$supplier", totalPrice: {$sum:"$price"}}}
])

db.fruits.aggregate([
	{$match: {"supplier" : "Red Farms Inc."}},
	{$group: {_id: "$supplier", "totalStocks" : {$sum: "$stocks"}}}
])

// average price of all items on sale

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group:{_id: "avgPriceOnSale", avgPrice: {$avg:"$price"}}}
])



// multiply operator

db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$project:{_id: "$name" , totalPrice: {$multiply: ["$price", "$stocks"]}}}
])

// $max allow us to get the highest value out of all the value of a given field

db.fruits.aggregate([
	{$match: {"onSale" : true}},
	{$group: {_id: "maxStocksOnSale", maxStock:{$max: "$stocks"}}}
])

// $min- gets the lowest value of the values in a given field
db.fruits.aggregate([
	{$match: {"onSale" : true}},
	{$group: {_id: "minStockOnSale", minStock: {$min: "$stocks"}}}
])

// other stages
	// count- is a stage we can add after a match stage to count all items

db.fruits.aggregate([
		{$match: {"onSale" : true}},
		{$count: "itemsOnSale"}
])

db.fruits.aggregate([
		{$match: {"price" : {$lt: 50}}},
		{$count: "priceLessThan50"}
])

db.fruits.aggregate([
		{$match: {"onSale" : false}},
		{$count: "priceOnSale"}
])

// count number of items per group
db.fruits.aggregate([
	{$match: {"onSale" : true}},
	{$group: {_id:"$supplier", noOfItems: {$sum : 1} }}
])

// you can have multiple field results per aggregated result

db.fruits.aggregate([
	{$match: {"onSale" : true}},
	{$group: {_id:"$supplier", noOfItems: {$sum : 1}, totalStocks: {$sum: "$stocks"} }}
])

// $out- save/output your aggregated result from $group in a new collection

db.fruits.aggregate([
	{$match: {"onSale" : true}},
	{$group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}} },
	{$out: "totalStocksPerSupplier"}

])


